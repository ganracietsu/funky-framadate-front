{
  "name": "framadate-funky-frontend",
  "version": "0.6.0",
  "license": "AGPL-3.0-or-later",
  "engines": {
    "node": ">= 16.13.0"
  },
  "resolutions": {
    "webpack": "^5.0.0"
  },
  "scripts": {
    "ng": "ng",
    "start": "ng serve",
    "start_docker": "ng serve --host 0.0.0.0",
    "build:prod": "ng build --configuration production",
    "build:prod:stats": "ng build --configuration production --stats-json",
    "build:prod:gitlabpage": "ng build --configuration production --baseHref=/framadate/funky-framadate-front/",
    "build:prod:demobliss": "ng build --configuration production --baseHref=https://framadate-api.cipherbliss.com  --stats-json ",
    "test": "jest",
    "test:watch": "jest --watch",
    "test:ci": "jest --ci",
    "lint": "prettier --write \"src/**/*.{js,jsx,ts,tsx,md,html,css,scss}\"",
    "e2e": "ng e2e",
    "release:major": "changelog -M && git add CHANGELOG.md && git commit -m 'updated CHANGELOG.md' && npm version major && git push origin && git push origin --tags",
    "release:minor": "changelog -m && git add CHANGELOG.md && git commit -m 'updated CHANGELOG.md' && npm version minor && git push origin && git push origin --tags",
    "release:patch": "changelog -p && git add CHANGELOG.md && git commit -m 'updated CHANGELOG.md' && npm version patch && git push origin && git push origin --tags",
    "format:check": "prettier --list-different \"src/{app,environments,assets}/**/*{.ts,.js,.json,.css,.scss}\"",
    "format:all": "prettier --write \"src/**/*.{js,jsx,ts,tsx,md,html,css,scss}\"",
    "trans": "ng xi18n --output-path=src/locale --i18n-locale=fr",
    "compodoc": "compodoc -p tsconfig.app.json",
    "mock:server": "json-server --port 8000 --watch ./mocks/db.json --routes ./mocks/routes.json",
    "start:proxy": "ng serve --proxy-config proxy.conf.json",
    "start:proxymock": "concurrently --kill-others \"yarn mock:server\" \"yarn start:proxy\"",
    "i18n:init": "ngx-translate-extract --input ./src --output ./src/assets/i18n/template.json --key-as-default-value --replace --format json",
    "i18n:extract": "ngx-translate-extract --input ./src --output ./src/assets/i18n/{en,da,de,fi,nb,nl,sv}.json --clean --format json"
  },
  "private": false,
  "dependencies": {
    "@angular/animations": "^15.2.2",
    "@angular/cdk": "^15.2.2",
    "@angular/common": "^15.2.2",
    "@angular/compiler": "^15.2.2",
    "@angular/core": "^15.2.2",
    "@angular/forms": "^15.2.2",
    "@angular/localize": "^15.2.2",
    "@angular/material": "^15.2.2",
    "@angular/platform-browser": "^15.2.2",
    "@angular/platform-browser-dynamic": "^15.2.2",
    "@angular/router": "^15.2.2",
    "@fullcalendar/core": "^6.1.4",
    "@ngx-translate/core": "^14.0.0",
    "@ngx-translate/http-loader": "^7.0.0",
    "angular-date-value-accessor": "^3.0.0",
    "axios": "^1.3.4",
    "bulma": "^0.9.0",
    "bulma-switch": "^2.0.0",
    "chart.js": "^4.2.1",
    "crypto": "^1.0.1",
    "crypto-js": "^4.0.0",
    "feather-icons": "^4.28.0",
    "fork-awesome": "^1.1.7",
    "marked": "^4.2.12",
    "moment": "^2.29.4",
    "ng-keyboard-shortcuts": "^13.0.8",
    "ng2-charts": "^4.1.1",
    "ngx-clipboard": "^15.1.0",
    "ngx-markdown": "^15.1.2",
    "ngx-webstorage": "^11.1.1",
    "node-forge": "^0.10.0",
    "primeicons": "^6.0.1",
    "primeng": "^15.2.0",
    "prismjs": "^1.29.0",
    "quill": "^1.3.7",
    "rxjs": "^6.5.5",
    "rxjs-compat": "^6.5.5",
    "short-unique-id": "^4.4.4",
    "stream": "^0.0.2",
    "tslib": "^2.0.0",
    "zone.js": "~0.11.4"
  },
  "devDependencies": {
    "@angular-builders/jest": "^9.0.1",
    "@angular-devkit/build-angular": "^15.2.3",
    "@angular/cli": "^15.2.3",
    "@angular/compiler-cli": "^15.2.2",
    "@angular/language-service": "^15.2.2",
    "@babel/core": "^7.9.0",
    "@babel/preset-env": "^7.9.5",
    "@babel/preset-typescript": "^7.9.0",
    "@compodoc/compodoc": "^1.1.11",
    "@types/crypto-js": "^4.0.0",
    "@types/jest": "^26.0.0",
    "@types/marked": "^4.0.8",
    "@types/node": "^14.0.1",
    "@typescript-eslint/eslint-plugin": "^3.0.0",
    "@typescript-eslint/parser": "^3.0.0",
    "@vendure/ngx-translate-extract": "^8.1.0",
    "babel-jest": "^26.0.0",
    "concurrently": "^5.2.0",
    "eslint": "^7.0.0",
    "eslint-config-prettier": "^6.11.0",
    "eslint-plugin-prettier": "^3.1.3",
    "husky": "^4.2.5",
    "jest": "^26.0.0",
    "jest-environment-jsdom-sixteen": "^1.0.3",
    "jest-preset-angular": "^8.1.3",
    "json-server": "^0.16.1",
    "lint-staged": "^10.1.7",
    "prettier": "^2.0.5",
    "protractor": "~7.0.0",
    "ts-jest": "^26.0.0",
    "ts-mockito": "^2.5.0",
    "ts-node": "^8.10.1",
    "typescript": "4.9.5"
  },
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "src/**/*.{js,jsx,ts,tsx,md,html,css,scss}": [
      "prettier --write",
      "git add"
    ],
    "*.js": [
      "prettier --write"
    ]
  },
  "jest": {
    "preset": "jest-preset-angular",
    "setupFilesAfterEnv": [
      "<rootDir>/src/jest.setup.ts"
    ],
    "testEnvironment": "jest-environment-jsdom-sixteen",
    "transform": {
      "^.+\\.(ts|html)$": "ts-jest",
      "^.+\\.jsx?$": "babel-jest"
    }
  },
  "browser": {
    "crypto": false
  }
}