import { Component, Input, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';

import { Answer } from '../../../core/enums/answer.enum';
import { Choice } from '../../../core/models/choice.model';
import { Poll } from '../../../core/models/poll.model';
import { ModalService } from '../../../core/services/modal.service';
import { ChoiceDetailsComponent } from '../../../shared/components/choice-details/choice-details.component';
import { StorageService } from '../../../core/services/storage.service';

@Component({
	selector: 'app-poll-results-compact',
	templateUrl: './poll-results-compact.component.html',
	styleUrls: ['./poll-results-compact.component.scss'],
})
export class PollResultsCompactComponent implements OnInit {
	@Input() public poll: Poll;
	public answerEnum = Answer;
	constructor(private modalService: ModalService, private storageService: StorageService) {}

	ngOnInit(): void {
		console.log('this.poll', this.poll);
	}

	public openModal(poll: Poll, choice: Choice): void {
		const config: MatDialogConfig<any> = { data: choice };
		this.modalService.openModal<ChoiceDetailsComponent, Choice>(ChoiceDetailsComponent, config);
	}

	toggleAnswer(choice_id: number, value: string) {
		this.storageService.toggleAnswer(choice_id, value);
	}

	showAsDate(date_string: string) {
		return new Date(date_string);
	}

	toggleAllOfChoice(groupe: any) {
		console.log('groupe', groupe);

		if (!groupe.subSetToYes) {
			this.storageService.setAllSubchoicesTo(groupe, 'yes');
			groupe.subSetToYes = true;
		} else {
			this.storageService.setAllSubchoicesTo(groupe, '');
			groupe.subSetToYes = false;
		}
		// savoir si on a déjà tout mis en "yes"
		// si oui, on enlève toutes les réponses
		// autrement on met tout à "yes"
	}
}
