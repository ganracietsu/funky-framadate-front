import { Component, Input, OnInit } from '@angular/core';
import { Choice } from '../../../core/models/choice.model';

@Component({
	selector: 'app-choice-table',
	templateUrl: './choice-table.component.html',
	styleUrls: ['./choice-table.component.scss'],
})
export class ChoiceTableComponent implements OnInit {
	@Input() detailledDisplay: boolean;
	@Input() choice: Choice;
	@Input() countMaxVotesForThisAnswer: number = 5;

	constructor() {}

	ngOnInit(): void {}

	roundToPercentWidth(number: number) {
		return Math.ceil((number / this.countMaxVotesForThisAnswer) * 100) + '%';
	}
}
